FROM ubuntu:22.04

MAINTAINER Vladimir Babushkin

#ENV TZ=Asia/Krasnoyarsk

RUN apt update -y && \
      apt-get install dpkg-dev devscripts equivs wget -y
COPY nginx-1.20.1 /home/master/

ENTRYPOINT [ "sleep", "240" ]